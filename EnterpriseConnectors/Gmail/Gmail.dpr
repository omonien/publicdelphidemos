program Gmail;

uses
  System.StartUpCopy,
  FMX.Forms,
  Forms.Main in 'Forms.Main.pas' {FormMain},
  Models.Gmail in 'Models.Gmail.pas' {ModelGmail: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TModelGmail, ModelGmail);
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end.
