library DXUDF;

uses
  System.SimpleShareMem,
  System.SysUtils,
  System.Classes,
  DX.IB.UDF.Misc in 'DX.IB.UDF.Misc.pas',
  DX.IB.SysUtils in 'DX.IB.SysUtils.pas';

{$R *.res}

exports
  HelloWorld,
  UpperCase,
  UpperCaseUTF8;
begin

end.
