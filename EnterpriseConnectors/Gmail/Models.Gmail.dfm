object ModelGmail: TModelGmail
  OldCreateOrder = False
  Height = 461
  Width = 439
  object ConnectionGMail: TFDConnection
    Params.Strings = (
      'User=Your GMail account login here'
      'Password=Your App Password here'
      'DriverID=CDataGmail')
    ConnectedStoredUsage = []
    LoginPrompt = False
    Left = 96
    Top = 96
  end
  object QInbox: TFDQuery
    Connection = ConnectionGMail
    SQL.Strings = (
      'select [ID], [From] , [To], [Subject], [Labels]'
      'from Inbox'
      'order by [Date] desc ')
    Left = 200
    Top = 96
    object QInboxID: TWideStringField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 510
    end
    object QInboxFrom: TWideStringField
      FieldName = 'From'
      Origin = '"From"'
      Size = 4000
    end
    object QInboxTo: TWideStringField
      FieldName = 'To'
      Origin = '"To"'
      Size = 4000
    end
    object QInboxSubject: TWideStringField
      FieldName = 'Subject'
      Origin = 'Subject'
      Size = 4000
    end
    object QInboxLabels: TWideStringField
      FieldName = 'Labels'
      Origin = 'Labels'
      Size = 4000
    end
  end
  object QDetails: TFDQuery
    MasterSource = DSInbox
    MasterFields = 'ID'
    Connection = ConnectionGMail
    SQL.Strings = (
      'select *'
      'from Inbox'
      'WHERE [ID] = :ID')
    Left = 200
    Top = 216
    ParamData = <
      item
        Name = 'ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object QDetailsId: TWideStringField
      FieldName = 'Id'
      Origin = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 510
    end
    object QDetailsMailbox: TWideStringField
      FieldName = 'Mailbox'
      Origin = 'Mailbox'
      Size = 4000
    end
    object QDetailsSubject: TWideStringField
      FieldName = 'Subject'
      Origin = 'Subject'
      Size = 4000
    end
    object QDetailsFullFrom: TWideStringField
      FieldName = 'FullFrom'
      Origin = 'FullFrom'
      Size = 4000
    end
    object QDetailsFrom: TWideStringField
      FieldName = 'From'
      Origin = '"From"'
      Size = 4000
    end
    object QDetailsFullTo: TWideStringField
      FieldName = 'FullTo'
      Origin = 'FullTo'
      Size = 4000
    end
    object QDetailsTo: TWideStringField
      FieldName = 'To'
      Origin = '"To"'
      Size = 4000
    end
    object QDetailsFullCC: TWideStringField
      FieldName = 'FullCC'
      Origin = 'FullCC'
      Size = 4000
    end
    object QDetailsCC: TWideStringField
      FieldName = 'CC'
      Origin = 'CC'
      Size = 4000
    end
    object QDetailsFullBCC: TWideStringField
      FieldName = 'FullBCC'
      Origin = 'FullBCC'
      Size = 4000
    end
    object QDetailsBCC: TWideStringField
      FieldName = 'BCC'
      Origin = 'BCC'
      Size = 4000
    end
    object QDetailsDate: TSQLTimeStampField
      FieldName = 'Date'
      Origin = '"Date"'
    end
    object QDetailsMessageBody: TWideStringField
      FieldName = 'MessageBody'
      Origin = 'MessageBody'
      Size = 16000
    end
    object QDetailsAttachments: TWideStringField
      FieldName = 'Attachments'
      Origin = 'Attachments'
      Size = 4000
    end
    object QDetailsAttachmentData: TWideStringField
      FieldName = 'AttachmentData'
      Origin = 'AttachmentData'
      Size = 16000
    end
    object QDetailsSize: TIntegerField
      FieldName = 'Size'
      Origin = '"Size"'
    end
    object QDetailsFlags: TWideStringField
      FieldName = 'Flags'
      Origin = 'Flags'
      Size = 4000
    end
    object QDetailsLabels: TWideStringField
      FieldName = 'Labels'
      Origin = 'Labels'
      Size = 4000
    end
    object QDetailsThreadId: TWideStringField
      FieldName = 'ThreadId'
      Origin = 'ThreadId'
      Size = 4000
    end
    object QDetailsMsgId: TWideStringField
      FieldName = 'MsgId'
      Origin = 'MsgId'
      Size = 4000
    end
    object QDetailsPartIds: TWideStringField
      FieldName = 'PartIds'
      Origin = 'PartIds'
      Size = 4000
    end
    object QDetailsPartFilenames: TWideStringField
      FieldName = 'PartFilenames'
      Origin = 'PartFilenames'
      Size = 4000
    end
    object QDetailsPartContentTypes: TWideStringField
      FieldName = 'PartContentTypes'
      Origin = 'PartContentTypes'
      Size = 4000
    end
    object QDetailsPartSizes: TWideStringField
      FieldName = 'PartSizes'
      Origin = 'PartSizes'
      Size = 4000
    end
    object QDetailsHeaders: TWideStringField
      FieldName = 'Headers'
      Origin = 'Headers'
      Size = 4000
    end
  end
  object DSInbox: TDataSource
    DataSet = QInbox
    Left = 200
    Top = 152
  end
end
