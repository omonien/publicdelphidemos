object ModelMain: TModelMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 451
  Width = 611
  object ClientPlaceholder: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'utf-8, *;q=0.8'
    BaseURL = 'https://jsonplaceholder.typicode.com/'
    Params = <>
    Left = 96
    Top = 64
  end
  object RequestPosts: TRESTRequest
    Client = ClientPlaceholder
    Params = <>
    Resource = 'posts'
    Response = ResponsePosts
    SynchronizedEvents = False
    Left = 200
    Top = 64
  end
  object AdpaterPosts: TRESTResponseDataSetAdapter
    Active = True
    Dataset = MTPosts
    FieldDefs = <>
    Response = ResponsePosts
    Left = 400
    Top = 64
  end
  object ResponsePosts: TRESTResponse
    ContentType = 'application/json'
    Left = 296
    Top = 64
  end
  object MTPosts: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'userId'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'id'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'title'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'body'
        DataType = ftWideString
        Size = 255
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 496
    Top = 64
    Content = {
      414442530F004B4FC6010000FF00010001FF02FF0304000E0000004D00540050
      006F0073007400730005000A0000005400610062006C00650006000000000007
      0000080032000000090000FF0AFF0B04000C0000007500730065007200490064
      0005000C0000007500730065007200490064000C00010000000E000D000F00FF
      00000010000111000112000113000114000115000116000C0000007500730065
      007200490064001700FF000000FEFF0B04000400000069006400050004000000
      690064000C00020000000E000D000F00FF000000100001110001120001130001
      140001150001180001160004000000690064001700FF000000FEFF0B04000A00
      00007400690074006C00650005000A0000007400690074006C0065000C000300
      00000E000D000F00FF0000001000011100011200011300011400011500011600
      0A0000007400690074006C0065001700FF000000FEFF0B04000800000062006F
      006400790005000800000062006F00640079000C00040000000E000D000F00FF
      00000010000111000112000113000114000115000116000800000062006F0064
      0079001700FF000000FEFEFF19FEFF1AFEFF1BFEFEFEFF1CFEFF1D1E00900100
      00FF1FFEFEFE0E004D0061006E0061006700650072001E005500700064006100
      7400650073005200650067006900730074007200790012005400610062006C00
      65004C006900730074000A005400610062006C00650008004E0061006D006500
      140053006F0075007200630065004E0061006D0065000A005400610062004900
      4400240045006E0066006F0072006300650043006F006E007300740072006100
      69006E00740073001E004D0069006E0069006D0075006D004300610070006100
      6300690074007900180043006800650063006B004E006F0074004E0075006C00
      6C00140043006F006C0075006D006E004C006900730074000C0043006F006C00
      75006D006E00100053006F007500720063006500490044001800640074005700
      69006400650053007400720069006E0067001000440061007400610054007900
      700065000800530069007A006500140053006500610072006300680061006200
      6C006500120041006C006C006F0077004E0075006C006C000800420061007300
      650014004F0041006C006C006F0077004E0075006C006C0012004F0049006E00
      55007000640061007400650010004F0049006E00570068006500720065001A00
      4F0072006900670069006E0043006F006C004E0061006D006500140053006F00
      7500720063006500530069007A0065000C004F0049006E004B00650079001C00
      43006F006E00730074007200610069006E0074004C0069007300740010005600
      6900650077004C006900730074000E0052006F0077004C006900730074001800
      520065006C006100740069006F006E004C006900730074001C00550070006400
      61007400650073004A006F00750072006E0061006C0012005300610076006500
      50006F0069006E0074000E004300680061006E00670065007300}
    object MTPostsuserId: TWideStringField
      FieldName = 'userId'
      Size = 255
    end
    object MTPostsid: TWideStringField
      FieldName = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 255
    end
    object MTPoststitle: TWideStringField
      FieldName = 'title'
      Size = 255
    end
    object MTPostsbody: TWideStringField
      FieldName = 'body'
      Size = 255
    end
  end
  object QPosts: TFDQuery
    Connection = Connection
    FetchOptions.AssignedValues = [evMode, evItems]
    UpdateOptions.AssignedValues = [uvUpdateChngFields, uvUpdateMode, uvLockMode, uvLockPoint, uvLockWait, uvRefreshMode, uvFetchGeneratorsPoint, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.UpdateChangedFields = False
    UpdateOptions.LockWait = True
    UpdateOptions.RefreshMode = rmManual
    UpdateOptions.FetchGeneratorsPoint = gpNone
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    SQL.Strings = (
      'Select * from Posts')
    Left = 160
    Top = 288
    object QPostsID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object QPostsUserID: TIntegerField
      FieldName = 'UserID'
      Origin = 'UserID'
      Required = True
    end
    object QPostsTitle: TStringField
      FieldName = 'Title'
      Origin = 'Title'
      Size = 200
    end
    object QPostsBody: TWideMemoField
      FieldName = 'Body'
      Origin = 'Body'
      BlobType = ftWideMemo
    end
  end
  object FDBatchMove1: TFDBatchMove
    Reader = FDBatchMoveDataSetReader1
    Writer = FDBatchMoveDataSetWriter1
    Mode = dmAppendUpdate
    Mappings = <
      item
        SourceFieldName = 'ID'
        DestinationFieldName = 'ID'
      end
      item
        SourceFieldName = 'UserID'
        DestinationFieldName = 'UserID'
      end
      item
        SourceFieldName = 'Title'
        DestinationFieldName = 'Title'
      end
      item
        SourceFieldName = 'Body'
        DestinationFieldName = 'Body'
      end>
    LogFileName = 'Data.log'
    Left = 488
    Top = 280
  end
  object FDBatchMoveDataSetReader1: TFDBatchMoveDataSetReader
    DataSet = MTPosts
    Left = 488
    Top = 200
  end
  object FDBatchMoveDataSetWriter1: TFDBatchMoveDataSetWriter
    DataSet = QPostsBatch
    Left = 488
    Top = 352
  end
  object QPostsBatch: TFDQuery
    Connection = Connection
    FetchOptions.AssignedValues = [evMode, evItems]
    FetchOptions.Mode = fmManual
    FetchOptions.Items = [fiMeta]
    UpdateOptions.AssignedValues = [uvUpdateChngFields, uvUpdateMode, uvLockMode, uvLockPoint, uvLockWait, uvRefreshMode, uvFetchGeneratorsPoint, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.UpdateChangedFields = False
    UpdateOptions.LockWait = True
    UpdateOptions.RefreshMode = rmManual
    UpdateOptions.FetchGeneratorsPoint = gpNone
    UpdateOptions.CheckRequired = False
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.CheckUpdatable = False
    SQL.Strings = (
      'Select * from Posts')
    Left = 352
    Top = 288
    object QPostsBatchID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IntegerField1: TIntegerField
      FieldName = 'UserID'
      Origin = 'UserID'
      Required = True
    end
    object StringField1: TStringField
      FieldName = 'Title'
      Origin = 'Title'
      Size = 200
    end
    object WideMemoField1: TWideMemoField
      FieldName = 'Body'
      Origin = 'Body'
      BlobType = ftWideMemo
    end
  end
  object Connection: TFDConnection
    Params.Strings = (
      'Database=C:\temp\placeholder.db'
      'LockingMode=Normal'
      'DriverID=SQLite')
    Connected = True
    LoginPrompt = False
    Left = 80
    Top = 288
  end
end
