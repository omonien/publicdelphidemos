unit Models.Main;

interface

uses
  System.SysUtils, System.Classes, REST.Types, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope,
  REST.Response.Adapter, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.DApt, FireDAC.Comp.BatchMove.DataSet, FireDAC.Comp.BatchMove,
  FireDAC.Stan.StorageBin;

type
  TModelMain = class(TDataModule)
    ClientPlaceholder: TRESTClient;
    RequestPosts: TRESTRequest;
    AdpaterPosts: TRESTResponseDataSetAdapter;
    ResponsePosts: TRESTResponse;
    MTPosts: TFDMemTable;
    QPosts: TFDQuery;
    QPostsID: TFDAutoIncField;
    QPostsUserID: TIntegerField;
    QPostsTitle: TStringField;
    QPostsBody: TWideMemoField;
    FDBatchMove1: TFDBatchMove;
    FDBatchMoveDataSetReader1: TFDBatchMoveDataSetReader;
    FDBatchMoveDataSetWriter1: TFDBatchMoveDataSetWriter;
    QPostsBatch: TFDQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    WideMemoField1: TWideMemoField;
    QPostsBatchID: TIntegerField;
    Connection: TFDConnection;
    MTPostsuserId: TWideStringField;
    MTPostsid: TWideStringField;
    MTPoststitle: TWideStringField;
    MTPostsbody: TWideStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure GetPosts;
    procedure RefreshDB;
    procedure BatchMove;
  end;

var
  ModelMain: TModelMain;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}
{$R *.dfm}

procedure TModelMain.DataModuleCreate(Sender: TObject);
begin
  RefreshDB;
end;

procedure TModelMain.BatchMove;
begin
  QPostsBatch.Close;
  QPostsBatch.FetchOptions.Mode := fmAll;
  QPostsBatch.Open;
  FDBatchMove1.Execute;
  QPostsBatch.Close;
end;

procedure TModelMain.GetPosts;
begin
  MTPosts.EmptyDataSet;
  RequestPosts.Execute;
end;

procedure TModelMain.RefreshDB;
begin
  QPosts.Close;
  QPosts.Open;
end;

end.
