object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'TV Show Demo'
  ClientHeight = 720
  ClientWidth = 432
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ListTVShows: TControlList
    Left = 0
    Top = 27
    Width = 432
    Height = 581
    Align = alClient
    ItemMargins.Left = 0
    ItemMargins.Top = 0
    ItemMargins.Right = 0
    ItemMargins.Bottom = 0
    ItemSelectionOptions.HotColorAlpha = 50
    ItemSelectionOptions.SelectedColorAlpha = 70
    ItemSelectionOptions.FocusedColorAlpha = 80
    ParentColor = False
    TabOrder = 0
    OnBeforeDrawItem = ListTVShowsBeforeDrawItem
    object LabelDescription: TLabel
      AlignWithMargins = True
      Left = 75
      Top = 25
      Width = 343
      Height = 38
      Margins.Left = 10
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Anchors = [akLeft, akTop, akRight, akBottom]
      AutoSize = False
      EllipsisPosition = epEndEllipsis
      ShowAccelChar = False
      Transparent = True
      WordWrap = True
    end
    object ImageTVShow: TVirtualImage
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 62
      Height = 62
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      ImageCollection = DMShows.ImagesTVShows
      ImageWidth = 0
      ImageHeight = 0
      ImageIndex = -1
    end
    object LabelTitle: TLabel
      Left = 75
      Top = 6
      Width = 3
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object EditSearch: TEdit
    Left = 0
    Top = 0
    Width = 432
    Height = 27
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    TextHint = 'Enter search term here ...'
    OnKeyUp = EditSearchKeyUp
  end
  object MemoLog: TMemo
    Left = 0
    Top = 608
    Width = 432
    Height = 89
    Align = alBottom
    TabOrder = 2
  end
  object CheckBoxShowLogs: TCheckBox
    AlignWithMargins = True
    Left = 3
    Top = 700
    Width = 426
    Height = 17
    Align = alBottom
    Caption = 'Show logs ...'
    TabOrder = 3
    OnClick = CheckBoxShowLogsClick
  end
end
