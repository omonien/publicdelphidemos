unit DX.IB.SysUtils;

interface

uses
  System.Classes, System.SysUtils, System.AnsiStrings;

// Imports for IB_Util library. Should be located in Interbase's "bin" directory
const
{$IF DEFINED(WIN64)}
  ib_Util_libraryname = 'ib_util64.dll';
{$ELSEIF DEFINED(MACOS) or DEfined (LINUX)}
  ib_Util_libraryname = 'libib_util.dylib'; // Not tested
{$ELSEIF}
{$MESSAGE fatal 'You should really run IB in 64 bit mode.'}
{$ENDIF}
  // Memory allocation as provided and required by Interbase
function ib_util_malloc(l: integer): pointer; cdecl; external ib_Util_libraryname;

// Converts a Delphi AnsiString to a properly allocated Interbase string
function StringToIBString(const AInput: AnsiString): PAnsiChar; overload;
// Converts a Delphi String to a properly allocated Interbase string, by encoding it to UTF8 first
function StringToIBString(const AInput: String): PAnsiChar; overload;

function DecodeFromUTF8(AInputUTF8: PAnsiChar): string;

implementation

function StringToIBString(const AInput: AnsiString): PAnsiChar;
begin
  result := ib_util_malloc(Length(AInput) + 1);
  System.AnsiStrings.StrPCopy(result, AInput);
end;

function StringToIBString(const AInput: String): PAnsiChar;
var
  LInputUTF8: TArray<Byte>;
  LInput: PAnsiChar;
  EncodedLength: integer;
begin
  LInputUTF8 := TEncoding.UTF8.GetBytes(AInput);
  EncodedLength := Length(LInputUTF8);
  result := ib_util_malloc(EncodedLength + 1);
  LInput := pointer(LInputUTF8);
  System.AnsiStrings.StrPCopy(result, LInput);
  result[EncodedLength] := #0;
end;

function DecodeFromUTF8(AInputUTF8: PAnsiChar): string;
var
  LInputBytes: TArray<Byte>;
  LInputLength: Cardinal;
begin
  LInputLength := System.AnsiStrings.StrLen(AInputUTF8);
  SetLength(LInputBytes, LInputLength);
  // Move is actually a copy operation...
  Move(AInputUTF8^, LInputBytes[0], LInputLength);
  result := TEncoding.UTF8.GetString(LInputBytes);

end;

initialization

IsMultiThread := true;

end.
