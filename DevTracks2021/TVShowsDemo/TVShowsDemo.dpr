program TVShowsDemo;

uses
  Vcl.Forms,
  Forms.Main in 'Forms.Main.pas' {FormMain},
  Modules.TVShows in 'Modules.TVShows.pas' {DMShows: TDataModule},
  Classes.TVShows in 'Classes.TVShows.pas';

{$R *.res}


begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TDMShows, DMShows);
  Application.Run;

end.
