object DMShows: TDMShows
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 407
  Width = 721
  object ClientTVMaze: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'utf-8, *;q=0.8'
    BaseURL = 'http://api.tvmaze.com/search'
    Params = <>
    Left = 80
    Top = 32
  end
  object RequestSearchShows: TRESTRequest
    AssignedValues = [rvConnectTimeout, rvReadTimeout]
    Client = ClientTVMaze
    Params = <
      item
        Name = 'q'
        Value = 'big bang'
      end>
    Resource = 'shows'
    Response = ResponseSearchShows
    Left = 80
    Top = 96
  end
  object ResponseSearchShows: TRESTResponse
    ContentType = 'application/json'
    Left = 80
    Top = 168
  end
  object ImagesTVShows: TImageCollection
    Images = <>
    Left = 352
    Top = 48
  end
end
