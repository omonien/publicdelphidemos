unit Forms.Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti, FMX.Grid.Style, FMX.ScrollBox, FMX.Grid,
  FMX.Controls.Presentation, FMX.StdCtrls, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope, Data.Bind.Controls, FMX.Layouts,
  Fmx.Bind.Navigator;

type
  TFormMain = class(TForm)
    Button1: TButton;
    StringGrid1: TStringGrid;
    BindSourceMemTable: TBindSourceDB;
    BindingsList1: TBindingsList;
    StringGrid2: TStringGrid;
    BindSourceQuery: TBindSourceDB;
    LinkGridToDataSourceBindSourceDB12: TLinkGridToDataSource;
    Button2: TButton;
    Button3: TButton;
    BindNavigator1: TBindNavigator;
    Layout1: TLayout;
    LinkGridToDataSourceBindSourceMemTable: TLinkGridToDataSource;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses
  Models.Main;

{$R *.fmx}

procedure TFormMain.Button1Click(Sender: TObject);
begin
  ModelMain.GetPosts;
end;

procedure TFormMain.Button2Click(Sender: TObject);
begin
  ModelMain.RefreshDB;
end;

procedure TFormMain.Button3Click(Sender: TObject);
begin
  ModelMain.BatchMove;
  ModelMain.RefreshDB;
end;

end.
