unit Forms.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.ImageList,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.VirtualImage, Vcl.ControlList,
  Vcl.ImgList, Vcl.VirtualImageList,
  Modules.TVShows, Data.Bind.EngExt, Vcl.Bind.DBEngExt, Vcl.Bind.ControlList, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope;

type
  TFormMain = class(TForm)
    CheckBoxShowLogs: TCheckBox;
    EditSearch: TEdit;
    ListTVShows: TControlList;
    MemoLog: TMemo;
    LabelDescription: TLabel;
    ImageTVShow: TVirtualImage;
    LabelTitle: TLabel;
    procedure CheckBoxShowLogsClick(Sender: TObject);
    procedure EditSearchKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure ListTVShowsBeforeDrawItem(AIndex: Integer; ACanvas: TCanvas; ARect: TRect; AState: TOwnerDrawState);
  public
    procedure UpdateLogVisibility;
    procedure UpdateList;
    procedure Log(const AMessage: string);
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}


procedure TFormMain.CheckBoxShowLogsClick(Sender: TObject);
begin
  UpdateLogVisibility;
end;

procedure TFormMain.EditSearchKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    DMShows.Search(EditSearch.Text);
    UpdateList;
  end;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  UpdateLogVisibility;
end;

procedure TFormMain.ListTVShowsBeforeDrawItem(AIndex: Integer; ACanvas: TCanvas; ARect: TRect; AState: TOwnerDrawState);
begin
  // This is called whenever TTcontrolList is about to render one of its items
  // if not DMShows.Shows[AIndex].Rendered then
  begin
    LabelTitle.Caption := DMShows.Shows[AIndex].Name;
    LabelDescription.Caption := DMShows.Shows[AIndex].SummaryShort;
    ImageTVShow.ImageName := 'Image' + DMShows.Shows[AIndex].Id;
    DMShows.Shows[AIndex].Rendered := true;
    Log('Show ' + DMShows.Shows[AIndex].Id + ' rendered');
  end
end;

procedure TFormMain.Log(const AMessage: string);
begin
  if CheckBoxShowLogs.Checked then
  begin
    MemoLog.Lines.Insert(0, AMessage);
  end;
end;

procedure TFormMain.UpdateList;
begin
  ListTVShows.ItemCount := 0;
  ListTVShows.ItemCount := DMShows.Shows.Count;
end;

procedure TFormMain.UpdateLogVisibility;
begin
  MemoLog.Visible := CheckBoxShowLogs.Checked;
end;

end.
