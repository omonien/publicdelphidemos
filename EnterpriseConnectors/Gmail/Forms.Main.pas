unit Forms.Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts,
  FMX.ListBox, FMX.ScrollBox, FMX.Memo, System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView;

type
  TFormMain = class(TForm)
    Layout1: TLayout;
    Label1: TLabel;
    ToolBar1: TToolBar;
    Button1: TButton;
    Layout2: TLayout;
    Memo1: TMemo;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    ListView1: TListView;
    LinkListControlToField1: TLinkListControlToField;
    BindSourceDB2: TBindSourceDB;
    LinkControlToField1: TLinkControlToField;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses
  Models.Gmail;

{$R *.fmx}

procedure TFormMain.Button1Click(Sender: TObject);
begin
  ModelGmail.Refresh;
end;

end.
