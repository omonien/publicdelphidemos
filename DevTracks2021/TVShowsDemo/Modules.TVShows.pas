unit Modules.TVShows;

interface

uses
  System.SysUtils, System.Classes, System.ImageList, System.Generics.Collections, System.JSON,
  Vcl.BaseImageCollection, Vcl.ImageCollection,
  REST.Types, REST.Client,
  Classes.TVShows, Data.Bind.Components, Data.Bind.ObjectScope;

type

  TDMShows = class(TDataModule)
    ClientTVMaze: TRESTClient;
    RequestSearchShows: TRESTRequest;
    ResponseSearchShows: TRESTResponse;
    ImagesTVShows: TImageCollection;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FShows: TShows;
  public
    property Shows: TShows read FShows;
    procedure Search(const ASearchTerm: string);
    procedure GetImages;
  end;

var
  DMShows: TDMShows;

implementation

uses
  RegularExpressions, System.Net.HttpClient, System.Threading;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


procedure TDMShows.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FShows);
end;

procedure TDMShows.GetImages;

  procedure GetImage(const AUrl: string; const AId: string);
  var
    LHttp: THTTPClient;
    LImageStream: TMemoryStream;
  begin
    TTAsk.Run(
      procedure
      begin
        LHttp := nil;
        LImageStream := nil;
        try
          LHttp := THTTPClient.Create;
          LImageStream := TMemoryStream.Create;
          try
            LHttp.Get(AUrl, LImageStream);
            TThread.Synchronize(nil,
              procedure
              begin
                ImagesTVShows.Add(AId, LImageStream);
              end);
          except
            // nothing, if http fails
          end;
        finally
          FreeAndNil(LImageStream);
          FreeAndNil(LHttp);
        end;
      end);
  end;

begin
  ImagesTVShows.Images.Clear;
  for var LShow in FShows do
  begin
    if LShow.ImageURL > '' then
    begin
      GetImage(LShow.ImageURL, 'Image' + LShow.Id);
    end;
  end;

end;

procedure TDMShows.DataModuleCreate(Sender: TObject);
begin
  FShows := TShows.Create;
end;

{ TDMShows }

procedure TDMShows.Search(const ASearchTerm: string);
begin
  RequestSearchShows.Params.ParameterByName('q').Value := ASearchTerm.trim;
  RequestSearchShows.Execute;
  FShows.Parse(ResponseSearchShows.JSONValue);
  GetImages;
end;

end.
