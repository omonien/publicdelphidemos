program RestToDatabase;

uses
  System.StartUpCopy,
  FMX.Forms,
  Forms.Main in 'Forms.Main.pas' {FormMain},
  Models.Main in 'Models.Main.pas' {ModelMain: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TModelMain, ModelMain);
  Application.Run;
end.
