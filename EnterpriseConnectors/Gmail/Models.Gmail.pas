unit Models.Gmail;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.CDataGmail,
  FireDAC.Phys.CDataGmailDef, FireDAC.FMXUI.Wait, Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TModelGmail = class(TDataModule)
    ConnectionGMail: TFDConnection;
    QInbox: TFDQuery;
    QInboxID: TWideStringField;
    QInboxFrom: TWideStringField;
    QInboxTo: TWideStringField;
    QInboxSubject: TWideStringField;
    QInboxLabels: TWideStringField;
    QDetails: TFDQuery;
    DSInbox: TDataSource;
    QDetailsId: TWideStringField;
    QDetailsMailbox: TWideStringField;
    QDetailsSubject: TWideStringField;
    QDetailsFullFrom: TWideStringField;
    QDetailsFrom: TWideStringField;
    QDetailsFullTo: TWideStringField;
    QDetailsTo: TWideStringField;
    QDetailsFullCC: TWideStringField;
    QDetailsCC: TWideStringField;
    QDetailsFullBCC: TWideStringField;
    QDetailsBCC: TWideStringField;
    QDetailsDate: TSQLTimeStampField;
    QDetailsMessageBody: TWideStringField;
    QDetailsAttachments: TWideStringField;
    QDetailsAttachmentData: TWideStringField;
    QDetailsSize: TIntegerField;
    QDetailsFlags: TWideStringField;
    QDetailsLabels: TWideStringField;
    QDetailsThreadId: TWideStringField;
    QDetailsMsgId: TWideStringField;
    QDetailsPartIds: TWideStringField;
    QDetailsPartFilenames: TWideStringField;
    QDetailsPartContentTypes: TWideStringField;
    QDetailsPartSizes: TWideStringField;
    QDetailsHeaders: TWideStringField;
  private
    { Private declarations }
  public
    procedure Refresh;
  end;

var
  ModelGmail: TModelGmail;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}
{$R *.dfm}

procedure TModelGmail.Refresh;
begin
  if QInbox.Active then
  begin
    QInbox.Refresh;
  end
  else
  begin
    QInbox.Open;
    QDetails.Open;
  end;
end;

end.
