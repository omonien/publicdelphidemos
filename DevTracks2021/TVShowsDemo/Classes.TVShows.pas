unit Classes.TVShows;

interface
uses System.SysUtils, System.Classes, System.ImageList, System.Generics.Collections, System.JSON,
Rest.Json;

type
  TShow = class(TObject)
  private
    FSummary: string;
    FId: string;
    FImageURL: string;
    FName: string;
    FRendered: boolean;
    FSummaryShort: string;
    procedure SetSummary(const Value: string);
  public
    constructor Create;
    property Id: string read FId write FId;
    property Summary: string read FSummary write SetSummary;
    property SummaryShort: string read FSummaryShort;
    property Name: string read FName write FName;
    property ImageURL: string read FImageURL write FImageURL;
    property Rendered: boolean read FRendered write FRendered;
  end;

  TShows = class(TObjectList<TShow>)
  public
    procedure Parse(ASearchResult: TJSONValue);
  end;

implementation

uses
  System.RegularExpressions;

{ TShow }

constructor TShow.Create;
begin
  inherited;
  FRendered := false;
end;

procedure TShow.SetSummary(const Value: string);
begin
  FSummary := Value;
  FSummaryShort := FSummary.Substring(0, 200);
end;

{ TShows }

procedure TShows.Parse(ASearchResult: TJSONValue);
begin
  if not(ASearchResult is TJSONArray) then
    raise Exception.Create('Unexpected response format!');
  Clear;
  for var LItem in TJSONArray(ASearchResult) do
  begin
    if not(LItem is TJSONObject) then
      raise Exception.Create('Unexpected response format!');

    var  LItemObject := TJSONObject(LItem);
    var  LShowObject := TJSONObject(LItemObject.FindValue('show'));
    var  LShow := TJson.JsonToObject<TShow>(LShowObject);
    var  LImageUrl := LShowObject.FindValue('image.original');
    if Assigned(LImageUrl) and LImageUrl.Value.ToLower.StartsWith('http') then
    begin
      LShow.ImageURL := LImageUrl.Value;
    end;
    // Remove HTML Tags
    LShow.Summary := TRegEx.Replace(LShow.Summary, '<[^>]*>', '');
    Add(LShow);
  end;
end;

end.
