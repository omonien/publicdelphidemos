unit DX.IB.UDF.Misc;

interface

uses
  System.Classes, System.SysUtils;

function HelloWorld(var AInput: integer): integer; cdecl; export;

function UpperCase(AInput: PAnsiChar): PAnsiChar; cdecl; export;

function UpperCaseUTF8(AInput: PAnsiChar): PAnsiChar; cdecl; export;

implementation

uses
  DX.IB.SysUtils;

function HelloWorld(var AInput: integer): integer;
begin
  result := 42 * AInput;
end;

function UpperCase(AInput: PAnsiChar): PAnsiChar;
var
  LResult: AnsiString;
  LInput: String;
begin
  LInput := String(AInput);
  LResult := AnsiString(System.SysUtils.UpperCase(LInput, loUserLocale));
  result := StringToIBString(LResult);
end;

function UpperCaseUTF8(AInput: PAnsiChar): PAnsiChar;
var
  LResult: String;
  LDecodedInput: String;
begin
  LDecodedInput := DecodeFromUTF8(AInput);
  LResult := System.SysUtils.UpperCase(LDecodedInput, loUserLocale);
  result := StringToIBString(LResult);
end;

end.
